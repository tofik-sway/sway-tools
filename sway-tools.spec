Name:           sway-tools
Version:        1.1.0
Release:        1%{?dist}
Summary:        Set of scripts and tools for SwayWM configuration

License:        GPL
URL:            https://gitlab.com/tofik-rpms/sway-tools
Source0:        https://gitlab.com/tofik-sway/sway-tools/-/archive/v%{version}/sway-tools-v%{version}.tar.gz

BuildArch:      noarch

Requires:       bemenu
Requires:       sway
Requires:       slurp
Requires:       grim
Requires:       swappy

%description
%{summary}

%prep
%setup -q -n %{name}-v%{version}

%build


%install
install -D -pm 755 src/sway-confirm %{buildroot}%{_bindir}/sway-confirm
install -D -pm 755 src/sway-screenshooter %{buildroot}%{_bindir}/sway-screenshooter
install -D -pm 755 src/sway-prop %{buildroot}%{_bindir}/sway-prop

%check


%files
%license LICENSE
%{_bindir}/*
%changelog
* Thu Sep 08 2022 Jerzy Drozdz <jerzy.drozdz@jdsieci.pl> - 1.1.0-1
- Added simple xprop replacement script

* Thu Sep 08 2022 Jerzy Drozdz <jerzy.drozdz@jdsieci.pl> - 1.0.1-1
- Added screenshooter script

* Thu Dec 02 2021 Jerzy Drozdz <jerzy.drozdz@jdsieci.pl> - 1.0.0-1
- Initial build
